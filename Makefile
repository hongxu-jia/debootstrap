# avoid dpkg-dev dependency; fish out the version with sed
VERSION := $(shell sed 's/.*(\(.*\)).*/\1/; q' debian/changelog)

all:

clean:

datadir ?= "/usr/share"
sbindir ?= "/usr/sbin"
DSDIR=$(DESTDIR)${datadir}/debootstrap
install:
	mkdir -p $(DSDIR)/scripts
	mkdir -p $(DESTDIR)$(sbindir)

	cp -a scripts/* $(DSDIR)/scripts/
	install -o root -g root -m 0644 functions $(DSDIR)/

	sed 's/@VERSION@/$(VERSION)/g' debootstrap >$(DESTDIR)$(sbindir)/debootstrap
	chown root:root $(DESTDIR)$(sbindir)/debootstrap
	chmod 0755 $(DESTDIR)$(sbindir)/debootstrap
